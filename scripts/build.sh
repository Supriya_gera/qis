#!/bin/bash

#if [ $# -ne 1 ]
#    then
#        echo "Invalid arguments, expected: [BRAND]"
#        exit
#fi

CWD=$(pwd)
ROOT=$(cd .. && pwd)

SRC="$ROOT/src"
DIST="$ROOT/dist"

# Returns a timestamp formatted as YYYY-MM-DD hh:mm:ss
function timestamp {
    echo "$(date +%Y-%m-%d) $(date +%H:%M:%S)"
}

# Runs SASS for given brand and device
function build {
    local BRAND="$1"

    echo
    echo "$(timestamp) Building for <brand:$BRAND>"
    echo

    sass --no-cache "$BRAND".sass "$DIST"/"$BRAND".css
}


mkdir -p "$DIST"
cd "$SRC"

# default (unbranded)
build default

# belfius (GEW7: BDN)
build belfius

# belins (GEWZ)
#build belins

# dvv (GEWU)
build dvv

# fo (GEFO: Front Office)
#build fo


echo
echo "All done!"
echo

cd $CWD
exit
